package com.epam.home_library;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Hello world!
 *
 */
public class HomeLibraryServer implements TCPConnectionListener
{
    public static void main( String[] args )
    {
        new HomeLibraryServer();
    }

    private HomeLibraryServer(){
        try(ServerSocket serverSocket = new ServerSocket(8000)) {
            while(true){
                new TCPConnection(serverSocket.accept(), this);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onQuery(String query, TCPConnection tcpConnection) {

    }

    @Override
    public void onDisconnection(TCPConnection tcpConnection) {
        System.out.println("User disconnected");
    }

    @Override
    public void onConnectionReady(TCPConnection tcpConnection) {
        System.out.println("Client is connected");
    }
}
