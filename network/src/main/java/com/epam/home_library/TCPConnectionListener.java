package com.epam.home_library;

public interface TCPConnectionListener {

    void onQuery(String query, TCPConnection tcpConnection);
    void onDisconnection(TCPConnection tcpConnection);
    void onConnectionReady(TCPConnection tcpConnection);
}
