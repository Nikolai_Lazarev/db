package com.epam.home_library;

import java.io.*;
import java.net.Socket;

public class TCPConnection {

    private Socket socket;
    private BufferedReader in;
    private BufferedWriter out;
    private TCPConnectionListener connectionListener;
    private Thread rxThread;

    TCPConnection(String ipAddr, int portAddr, TCPConnectionListener connectionListener)throws IOException{
        this(new Socket(ipAddr, portAddr), connectionListener);
    }

    TCPConnection(final Socket socket, final TCPConnectionListener connectionListener) throws IOException {
        this.socket = socket;
        this.connectionListener = connectionListener;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        rxThread = new Thread(new Runnable() {
            @Override
            public void run() {
                connectionListener.onConnectionReady(TCPConnection.this);
                while (!rxThread.isInterrupted()) {
                    try {
                        connectionListener.onQuery(in.readLine(), TCPConnection.this);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        connectionListener.onDisconnection(TCPConnection.this);
                    }
                }
            }
        });
        rxThread.start();
    }

}
