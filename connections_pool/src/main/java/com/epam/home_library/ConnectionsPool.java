package com.epam.home_library;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;

/**
 * Hello world!
 *
 */
public class ConnectionsPool
{
    ConnectionsPool(int capacity){
        ArrayList<Connection> connections = new ArrayList<Connection>(capacity);
        for(Connection x : connections){
            x = DriverManager.getConnection();
        }
    }
}
