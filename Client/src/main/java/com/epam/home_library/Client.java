package com.epam.home_library;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class Client implements TCPConnectionListener
{
    public static void main( String[] args )
    {
        new Client();
    }

    private Client(){
        try {
            new TCPConnection("127.0.0.1", 8000, this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onQuery(String query, TCPConnection tcpConnection) {

    }

    @Override
    public void onDisconnection(TCPConnection tcpConnection) {

    }

    @Override
    public void onConnectionReady(TCPConnection tcpConnection) {

    }
}
